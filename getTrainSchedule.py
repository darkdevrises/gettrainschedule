import urllib.request
from bs4 import BeautifulSoup
import csv


def getScheduleAsCSV(inputTrain):
    
    inputTrain = str(inputTrain)
    
    trainManGenericUrl = "https://www.trainman.in/train/"
    
    inputTrainUrl = trainManGenericUrl + inputTrain
    
    outputFile = inputTrain + ".csv"
    
    f = open(outputFile, 'w', newline = '')
    writer = csv.writer(f)
    
    soup = BeautifulSoup(urllib.request.urlopen(inputTrainUrl).read(), 'lxml')
    
    tbody = soup('table', {"id":"scheduleTable"})[0].find_all('tr')
    
    for row in tbody:
        cols = row.findChildren(recursive=False)
        cols = [ele.text.strip() for ele in cols]
        writer.writerow(cols)
        
    
    f.close()    
    
if __name__ == "__main__":
    inputTrainList = [75901, 75902, 75903, 75904, 75905, 75906, 75907, 75908]
    
    for inputTrain in inputTrainList:
        getScheduleAsCSV(inputTrain)


